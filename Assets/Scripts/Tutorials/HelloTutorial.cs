﻿using System.Collections;
using Game.ScenarioSystem.GuiHighlight;
using Geo.UI;
using SiberianWellness.NotNullValidation;
using UnityEngine; 

namespace Geo.Tutorials
{
	public class HelloTutorial : MonoBehaviour
	{
		[SerializeField, IsntNull]
		TutorialFacade facade;
		 
		[SerializeField, IsntNull]
		TutorialGui tutorialGui;

		[SerializeField, IsntNull]
		RecentFilesPopup recentFilesPopup;
		
		[SerializeField, IsntNull]
		RectTransform documentButtonRect, settingsButtonRect;

		[Header("SaveImage")]
		[SerializeField, IsntNull]
		Sprite tutorialSprite;
		
		public IEnumerator Execute()
		{
			Debug.Log("[tutorial] "+"1");
			
			tutorialGui.ShowText("Привет, меня зовут Барт");
			tutorialGui.HideContent();
			yield return tutorialGui.WaitForClick();
			
			tutorialGui.ShowText("Покажу тебе, что умеет приложение 'GeoKPT'");
			tutorialGui.HideContent();
			yield return tutorialGui.WaitForClick();
			yield return tutorialGui.SetAlpha(1,0);

			tutorialGui.ShowText("Сверху ты видешь список доступных кадастровых планов территорий (КПТ)");
			yield return tutorialGui.SetAlpha(0,1);
			yield return tutorialGui.WaitForClick();
			
			tutorialGui.ShowText("Я добавил эти КПТ для примера, чтобы показать тебе как пользоваться приложением");
			yield return tutorialGui.WaitForClick();
			yield return tutorialGui.SetAlpha(1,0);
			
			
			RecentFileButton button = recentFilesPopup.Buttons[0];
			tutorialGui.ShowText("Посмотрим какие участки имеются в КПТ города Томск", button.Root, ArrowOrientation.FromDown);
			yield return tutorialGui.SetAlpha(0, 1);
			
			var wait = facade.WaitForPopupOpened<DocumentPopup>();
			yield return wait;
			tutorialGui.Hide();
			
			//---Выбор участка
			facade.SetEnableInput(false);
			var waitForLoad = new WaitForEvent<string>();
			wait.Popup.successLoad += waitForLoad.Complete;
			yield return waitForLoad;
			wait.Popup.successLoad -= waitForLoad.Complete;
			facade.SetEnableInput(true);
			wait.Popup.DeselectInput();
			
			tutorialGui.ShowText("Отлично");
			yield return tutorialGui.SetAlpha(0, 1);
			yield return tutorialGui.WaitForClick();
			wait.Popup.DeselectInput();
			
			tutorialGui.ShowText("Это список участков");
			yield return tutorialGui.WaitForClick();
			
			tutorialGui.ShowText("Сверху есть удобный поисковик");
			yield return tutorialGui.WaitForClick();
			
			tutorialGui.ShowText("Можно искать участки по части кадастрового номера или по адресу");
			yield return tutorialGui.WaitForClick();
			
			tutorialGui.ShowText("Выбери любой участок, чтобы увидеть его координаты");
			yield return tutorialGui.WaitForClick();
			
			yield return tutorialGui.SetAlpha(1, 0);
			tutorialGui.Hide();
			
			//---Информация об участке
			var wait2 = facade.WaitForPopupOpened<SaveContourPopup>();
			yield return wait2;
			
			tutorialGui.ShowText("Супер");
			yield return tutorialGui.SetAlpha(0, 1);
			yield return tutorialGui.WaitForClick(); 
			
			tutorialGui.ShowText("Видно координаты участка и другую информацию");
			yield return tutorialGui.WaitForClick();
			
			yield return tutorialGui.WaitForReady("Изучи экран и, когда будешь готов продолжить, тапни сюда");
			yield return tutorialGui.SetAlpha(1, 0);
			  
			//---Идем в настройки
			tutorialGui.ShowText("Возможно, ты хочешь изменить формат координат");
			yield return tutorialGui.SetAlpha(0, 1);
			yield return tutorialGui.WaitForClick();
			  
			tutorialGui.ShowText("Формат изменяется в настройках", settingsButtonRect, ArrowOrientation.FromTop);
			yield return tutorialGui.SetAlpha(0, 1);
			var wait3 = facade.WaitForPopupOpened<SettingsPopup>();
			yield return wait3;
			tutorialGui.Hide();
			
			yield return tutorialGui.WaitForReady("Если хочешь, измени настойки формата, а потом тапни сюда, чтобы продолжить обучение");
			yield return tutorialGui.SetAlpha(1, 0);
			
			tutorialGui.ShowText("Вернемся к участку", documentButtonRect, ArrowOrientation.FromTop);
			yield return tutorialGui.SetAlpha(0, 1);
			var wait4 = facade.WaitForPopupOpened<SaveContourPopup>();
			yield return wait4;
			tutorialGui.Hide();
			
			//--После настроек формата. Сохраняем.
			tutorialGui.ShowText("Давай сохраним координаты в файл");
			yield return tutorialGui.SetAlpha(0, 1);
			yield return tutorialGui.WaitForClick();
			 
			yield return tutorialGui.ShowSprite("Возможно, потребуется включить видимость хранилища", tutorialSprite);
			yield return tutorialGui.SetAlpha(1, 0);
			
			RectTransform buttonRect = wait4.Popup.SaveToFileButton.GetComponent<RectTransform>();
			tutorialGui.ShowText("Давай сохраним координаты в файл", buttonRect , ArrowOrientation.FromTop);
			yield return tutorialGui.SetAlpha(0, 1);

			var waitForSave = new WaitForEvent<string>();
			wait4.Popup.saved += waitForSave.Complete;
			yield return waitForSave;
			wait4.Popup.saved -= waitForSave.Complete;
			
			tutorialGui.ShowText("Хорошо");
			tutorialGui.HideContent();
			yield return tutorialGui.WaitForClick();
			
			tutorialGui.ShowText("Теперь можешь скачать файлы КПТ на телефон и в любой момент получить координаты нужного участка");
			tutorialGui.HideContent();
			yield return tutorialGui.WaitForClick();
			
			tutorialGui.ShowText("До встречи");
			tutorialGui.HideContent();
			yield return tutorialGui.WaitForClick();
			yield return tutorialGui.SetAlpha(1, 0);
			tutorialGui.Hide();
		}
	}
}
