﻿using System;
using System.Collections.Generic;
using Geo.Data.AnalyticsDb;

namespace Geo.Data
{
	public class AccountData
	{
		[Serializable]
		public class Metadata
		{
			public string appVersion;
			public string writeLocalTime;

			public void FillData()
			{
				appVersion = appVersion;
				writeLocalTime = DateTime.Now.ToString("G");
			}
		}

		

		/// <summary>
		/// Никогда нельзя изменять имя переменной, ее тип или удалять ее!
		/// </summary>
		public Metadata metadata = new Metadata();

		public List<string> lastOpenedFilePaths = new List<string>();
		public AppAnalyticsDb appAnalytics = new AppAnalyticsDb();
	}
}